from django.test import TestCase
from  django.contrib.auth.models import User
from .models import Post
# Create your tests here.


class BlogTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        testuser1 = User.objects.create_user(
            username='nabin', password='1997'
        )
        testuser1.save()

        # ctreate blog post

        blogpost = Post.objects.create(
            author=testuser1,
            title='hello world',
            body='this is hello world',

        )
        blogpost.save()

    def test_blog_content(self):
        post = Post.objects.get(id=1)
        author =f'{post.author}'
        title = f'{post.title}'
        body = f'{post.body}'

        self.assertEqual(author, 'nabin')
        self.assertEqual(title, 'hello world')
        self.assertEqual(body, 'this is hello world')